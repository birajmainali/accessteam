﻿using System;
using System.Threading.Tasks;
using Client.Repository.Interface;
using Client.ValidationHelper.Interface;

namespace Client.ValidationHelper
{
    public class ClientValidator : IClientValidator
    {
        private readonly IClientRepository _clientRepository;

        public ClientValidator(IClientRepository clientRepository)
        {
            _clientRepository = clientRepository;
        }

        public async Task CheckDuplicateClientName(string name, long? id = null)
        {
            if (await _clientRepository.IsClientNameUsed(name, id)) throw new Exception("Duplication Client Name");
        }
    }
}