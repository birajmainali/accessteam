﻿using System.Threading.Tasks;

namespace Client.ValidationHelper.Interface
{
    public interface IClientValidator
    {
        Task CheckDuplicateClientName(string name, long? id = null);
    }
}