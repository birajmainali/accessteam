﻿using Base.Constants;
using Microsoft.EntityFrameworkCore;

namespace Client
{
    public static class EntityRegisterer
    {
        public static ModelBuilder AddClient(this ModelBuilder builder)
        {
            builder.Entity<Entity.Client>().ToTable("client_info", Schema.Client);
            return builder;
        }
    }
}