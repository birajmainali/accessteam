﻿using Base.Entity;

namespace Client.Entity
{
    public class Client : BaseEntity
    {
        protected Client()
        {
        }

        public Client(string name, string contact, string manager, string address)
        {
            Copy(name, contact, manager, address);
        }

        public string Name { get; protected set; }
        public string Contact { get; protected set; }
        public string Manager { get; protected set; }
        public string Address { get; protected set; }

        private void Copy(string name, string contact, string manager, string address)
        {
            Name = name;
            Contact = contact;
            Manager = manager;
            Address = address;
        }

        public void Update(string name, string contact, string manager, string address)
        {
            Copy(name, contact, manager, address);
        }
    }
}