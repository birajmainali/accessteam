﻿using Client.Repository;
using Client.Repository.Interface;
using Client.Services;
using Client.Services.Interface;
using Client.ValidationHelper;
using Client.ValidationHelper.Interface;
using Microsoft.Extensions.DependencyInjection;

namespace Client
{
    public static class DiConfig
    {
        public static IServiceCollection UseClient(this IServiceCollection service)
        {
            return service.AddScoped<IClientRepository, ClientRepository>()
                .AddScoped<IClientValidator, ClientValidator>()
                .AddScoped<IClientServices, ClientServices>();
        }
    }
}