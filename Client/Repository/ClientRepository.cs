﻿using System.Threading.Tasks;
using Base.GenericRepository;
using Client.Repository.Interface;
using Microsoft.EntityFrameworkCore;

namespace Client.Repository
{
    public class ClientRepository : GenericRepository<Entity.Client>, IClientRepository
    {
        public ClientRepository(DbContext context) : base(context)
        {
        }

        public async Task<bool> IsClientNameUsed(string name, long? excludingId = null)
        {
            return await CheckIfExistAsync(x =>
                (excludingId == null || x.Id != excludingId) && x.Name.Trim().ToLower() == name.Trim().ToLower());
        }
    }
}