﻿using System.Threading.Tasks;
using Base.GenericRepository.Interface;

namespace Client.Repository.Interface
{
    public interface IClientRepository : IGenericRepository<Entity.Client>
    {
        Task<bool> IsClientNameUsed(string client, long? excludingId = null);
    }
}