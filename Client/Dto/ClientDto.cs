﻿namespace Client.Dto
{
    public class ClientDto
    {
        public string Name { get; set; }
        public string Contact { get; set; }
        public string Manager { get; set; }
        public string Address { get; set; }
    }
}