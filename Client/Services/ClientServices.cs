﻿using System.Threading.Tasks;
using System.Transactions;
using Client.Dto;
using Client.Repository.Interface;
using Client.Services.Interface;
using Client.ValidationHelper.Interface;
using ClientEntity = Client.Entity.Client;

namespace Client.Services
{
    public class ClientServices : IClientServices
    {
        private readonly IClientRepository _clientRepository;
        private readonly IClientValidator _clientValidator;

        public ClientServices(IClientRepository clientRepository, IClientValidator clientValidator)
        {
            _clientRepository = clientRepository;
            _clientValidator = clientValidator;
        }

        public async Task Create(ClientDto dto)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            await _clientValidator.CheckDuplicateClientName(dto.Name);
            var client = new ClientEntity(dto.Name.Trim(), dto.Contact.Trim(), dto.Manager.Trim(), dto.Address.Trim());
            await _clientRepository.CreateAsync(client);
            await _clientRepository.FlushAsync();
            tsc.Complete();
        }

        public async Task Edit(ClientEntity client, ClientEditDto dto)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            await _clientValidator.CheckDuplicateClientName(dto.Name.Trim(), client.Id);
            client.Update(dto.Name.Trim(), dto.Contact.Trim(), dto.Manager.Trim(), dto.Address.Trim());
            _clientRepository.Update(client);
            await _clientRepository.FlushAsync();
            tsc.Complete();
        }

        public async Task Remove(ClientEntity client)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            _clientRepository.Remove(client);
            await _clientRepository.FlushAsync();
            tsc.Complete();
        }
    }
}