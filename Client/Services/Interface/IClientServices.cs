﻿using System.Threading.Tasks;
using Client.Dto;

namespace Client.Services.Interface
{
    public interface IClientServices
    {
        Task Create(ClientDto dto);
        Task Edit(Entity.Client client, ClientEditDto dto);
        Task Remove(Entity.Client client);
    }
}