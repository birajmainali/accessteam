﻿namespace Base.Constants
{
    public static class Status
    {
        public const string Active = "Active";
        public const string InActive = "Inactive";
        public const char RecA = 'A';
        public const char RecD = 'D';
    }
}