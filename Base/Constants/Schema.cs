﻿namespace Base.Constants
{
    public static class Schema
    {
        public const string Client = "Client";
        public const string General = "General";
        public const string Team = "Team";
        public const string Work = "Work";
        public const string User = "User";
    }
}