﻿using Base.Constants;
using Microsoft.EntityFrameworkCore;

namespace Work
{
    public static class EntityRegisterer
    {
        public static ModelBuilder AddWork(this ModelBuilder builder)
        {
            builder.Entity<Entity.Work>().ToTable("works", Schema.Work);
            return builder;
        }
    }
}