﻿using Base.GenericRepository.Interface;

namespace Work.Repository.Interface
{
    public interface IWorkRepository : IGenericRepository<Entity.Work>
    {
    }
}