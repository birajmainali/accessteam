﻿using Base.GenericRepository;
using Microsoft.EntityFrameworkCore;
using Work.Repository.Interface;

namespace Work.Repository
{
    public class WorkRepository : GenericRepository<Entity.Work>, IWorkRepository
    {
        public WorkRepository(DbContext context) : base(context)
        {
        }
    }
}