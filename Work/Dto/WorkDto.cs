﻿using System;

namespace Work.Dto
{
    public class WorkDto
    {
        public string ClientName { get; set; }
        public string Assign { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime SolvedDate { get; set; }
        public string Solved { get; set; }
        public string TaskSource { get; set; }
        public string StartedDate { get; set; }
        public string UserId { get; set; }
    }
}