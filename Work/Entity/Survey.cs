﻿using Base.Entity;

namespace Work.Entity
{
    public class Survey : BaseEntity
    {
        public string Title { get; set; }
        public string? Description { get; set; }
    }
}