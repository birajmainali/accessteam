﻿using Base.Entity;

namespace Work.Entity
{
    public class OverTimeData : BaseEntity
    {
        public string Client { get; set; }
        public Work Work { get; set; }
    }
}