﻿using System;
using Base.Entity;

namespace Work.Entity
{
    public class Work : BaseEntity
    {
        public virtual Client.Entity.Client Client { get; set; }
        public string Assign { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime SolvedDate { get; set; }
        public string Solved { get; set; }
        public string TaskSource { get; set; }
        public string StartedDate { get; set; }
    }
}