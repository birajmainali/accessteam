﻿using Microsoft.Extensions.DependencyInjection;
using Work.Repository;
using Work.Repository.Interface;

namespace Work
{
    public static class DiConfig
    {
        public static IServiceCollection UseWork(this IServiceCollection service)
        {
            return service.AddScoped<IWorkRepository, WorkRepository>();
        }
    }
}