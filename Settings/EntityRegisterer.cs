﻿using Base.Constants;
using Microsoft.EntityFrameworkCore;
using Settings.Entity;

namespace Settings
{
    public static class EntityRegisterer
    {
        public static ModelBuilder AddSettings(this ModelBuilder builder)
        {
            builder.Entity<Product>().ToTable("product_setup", Schema.General);
            builder.Entity<Survey>().ToTable("survey_setup", Schema.General);
            builder.Entity<Module>().ToTable("module_setup", Schema.General);
            builder.Entity<TaskSetup>().ToTable("task_setup", Schema.General);
            return builder;
        }
    }
}