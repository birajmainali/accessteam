﻿using System.Threading.Tasks;

namespace Settings.ValidationHelper.Interface
{
    public interface ISurveyValidator
    {
        Task CheckIfAlreadyPublish(long id);
    }
}