﻿using System.Threading.Tasks;

namespace Settings.ValidationHelper.Interface
{
    public interface IProductValidator
    {
        Task CheckDuplicatesProductName(string name, long? id = null);

        Task CheckProductIsInUse(long id);
    }
}