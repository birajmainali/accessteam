﻿using System.Threading.Tasks;

namespace Settings.ValidationHelper.Interface
{
    public interface IModuleServiceValidator
    {
        Task CheckUniqueModuleName(string name, string product);
    }
}