﻿using System;
using System.Threading.Tasks;
using Settings.Repository.Interface;
using Settings.ValidationHelper.Interface;

namespace Settings.ValidationHelper
{
    public class SurveyValidator : ISurveyValidator
    {
        private readonly ISurveyRepository _surveyRepository;

        public SurveyValidator(ISurveyRepository surveyRepository)
        {
            _surveyRepository = surveyRepository;
        }

        public async Task CheckIfAlreadyPublish(long id)
        {
            if (await _surveyRepository.CheckIfExistAsync(x => x.Id == id && x.IsPublish))
                throw new Exception("Form Already Published");
        }
    }
}