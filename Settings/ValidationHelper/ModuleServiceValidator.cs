﻿using System;
using System.Threading.Tasks;
using Settings.Repository.Interface;
using Settings.ValidationHelper.Interface;

namespace Settings.ValidationHelper
{
    public class ModuleServiceValidator : IModuleServiceValidator
    {
        private readonly IModuleRepository _moduleRepository;

        public ModuleServiceValidator(IModuleRepository moduleRepository)
        {
            _moduleRepository = moduleRepository;
        }

        public async Task CheckUniqueModuleName(string name, string product)
        {
            if (await _moduleRepository.CheckIfExistAsync(x =>
                x.Name.Trim().ToLower() == name.ToLower().Trim() &&
                x.Product.Name.Trim().ToLower() == product.Trim().ToLower()))
            {
                throw new Exception("Duplicate Module Name");
            }
        }
    }
}