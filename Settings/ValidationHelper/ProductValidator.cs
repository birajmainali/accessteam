﻿using System;
using System.Threading.Tasks;
using Settings.Repository.Interface;
using Settings.ValidationHelper.Interface;

namespace Settings.ValidationHelper
{
    public class ProductValidator : IProductValidator
    {
        private readonly IProductRepository _productRepository;
        private readonly IModuleRepository _moduleRepository;

        public ProductValidator(IProductRepository productRepository, IModuleRepository moduleRepository)
        {
            _productRepository = productRepository;
            _moduleRepository = moduleRepository;
        }

        public async Task CheckDuplicatesProductName(string name, long? id = null)
        {
            if (await _productRepository.CheckIfProductIsUsed(name, id))
                throw new Exception("Product Name Must Be Unique, for Accessibility");
        }

        public async Task CheckProductIsInUse(long id)
        {
            var product = await _productRepository.FindOrThrowAsync(id);
            if (await _moduleRepository.CheckIfExistAsync(x => x.ProductId == id))
            {
                throw new Exception($"Can not remove {product.Name},Product is in use");
            }
        }
    }
}