﻿using Base.Entity;

namespace Settings.Entity
{
    public class TaskSetup : BaseEntity
    {
        public string Type { get; protected set; }
        public string Description { get; protected set; }
        public long SuccessPoint { get; protected set; }
        public virtual Module Module { get; protected set; }
        public long ModuleId { get; set; }
        protected TaskSetup()
        {
        }

        private void Copy(string type, string description, long point, Module module)
        {
            Type = type;
            Description = description;
            SuccessPoint = point;
            Module = module;
        }

        public TaskSetup(string type, string description, long point, Module module)
        {
            Copy(type, description, point, module);
        }

        public void Update(string type, string description, long point, Module module)
        {
            Copy(type, description, point, module);
        }
    }
}