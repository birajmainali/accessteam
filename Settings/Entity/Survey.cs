﻿using System;
using Base.Entity;

namespace Settings.Entity
{
    public class Survey : BaseEntity
    {
        protected Survey()
        {
        }

        public Survey(string title, string description, User.Entity.User user)
        {
            Copy(title, description, user);
        }

        public string Title { get; protected set; }
        public string Description { get; protected set; }

        public bool IsPublish { get; set; } = false;

        public DateTime? PublishDate { get; set; } = null;

        public DateTime? DueDate { get; set; } = null;
        public virtual User.Entity.User User { get; protected set; }
        public long UserId { get; }

        private void Copy(string title, string description, User.Entity.User user)
        {
            Title = title;
            Description = description;
            User = user;
        }

        public void Update(string title, string description, User.Entity.User user)
        {
            Copy(title, description, user);
        }
    }
}