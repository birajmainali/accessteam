﻿using Base.Entity;

namespace Settings.Entity
{
    public class Module : BaseEntity
    {
        public string Name { get; protected set; }
        public long ProductId { get; set; }
        public virtual Product Product { get; protected set; }


        protected Module()
        {
        }

        private void Copy(string name, Product product)
        {
            Name = name;
            Product = product;
        }

        public Module(string name, Product product)
        {
            Copy(name, product);
        }

        public void Update(string name, Product product)
        {
            Copy(name, product);
        }
    }
}