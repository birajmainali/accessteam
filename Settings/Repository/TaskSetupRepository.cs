﻿using Base.GenericRepository;
using Microsoft.EntityFrameworkCore;
using Settings.Entity;
using Settings.Repository.Interface;

namespace Settings.Repository
{
    public class TaskSetupRepository : GenericRepository<TaskSetup>, ITaskSetupRepository
    {
        public TaskSetupRepository(DbContext context) : base(context)
        {
        }
    }
}