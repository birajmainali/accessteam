﻿using Base.GenericRepository;
using Microsoft.EntityFrameworkCore;
using Settings.Entity;
using Settings.Repository.Interface;

namespace Settings.Repository
{
    public class SurveyRepository : GenericRepository<Survey>, ISurveyRepository
    {
        public SurveyRepository(DbContext context) : base(context)
        {
        }
    }
}