﻿using System.Threading.Tasks;
using Base.GenericRepository.Interface;
using Settings.Entity;

namespace Settings.Repository.Interface
{
    public interface IProductRepository : IGenericRepository<Product>
    {
        Task<bool> CheckIfProductIsUsed(string product, long? excludedId = null);
    }
}