﻿using Base.GenericRepository.Interface;
using Settings.Entity;

namespace Settings.Repository.Interface
{
    public interface ISurveyRepository : IGenericRepository<Survey>
    {
    }
}