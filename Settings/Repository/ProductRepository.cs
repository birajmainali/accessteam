﻿using System.Threading.Tasks;
using Base.GenericRepository;
using Microsoft.EntityFrameworkCore;
using Settings.Entity;
using Settings.Repository.Interface;

namespace Settings.Repository
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(DbContext context) : base(context)
        {
        }

        public async Task<bool> CheckIfProductIsUsed(string product, long? excludedId = null)
        {
            return await CheckIfExistAsync(x =>
                (excludedId == null || x.Id != excludedId) && x.Name.ToLower() == product.ToLower());
        }
    }
}