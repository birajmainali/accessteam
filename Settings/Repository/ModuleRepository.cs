﻿using Base.GenericRepository;
using Microsoft.EntityFrameworkCore;
using Settings.Entity;
using Settings.Repository.Interface;

namespace Settings.Repository
{
    public class ModuleRepository : GenericRepository<Module>, IModuleRepository
    {
        public ModuleRepository(DbContext context) : base(context)
        {
        }
    }
}