using System.Collections.Generic;
using Settings.Entity;

// suii
namespace Settings.Dto
{
    public record BulkModuleAddDto(Product Product, List<string> Modules);
}
