﻿using Settings.Entity;

namespace Settings.Dto
{
    public class TaskSetupDto
    {
        public string Type { get; set; }
        public string Description { get; set; }
        public long SuccessPoint { get; set; }
        public Module Module { get; set; }
    }
}