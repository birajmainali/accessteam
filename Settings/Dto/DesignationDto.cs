﻿namespace Settings.Dto
{
    public class DesignationDto
    {
        public string Post { get; set; }
        public string Description { get; set; }
    }
}