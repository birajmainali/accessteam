﻿namespace Settings.Dto
{
    public class ProductDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}