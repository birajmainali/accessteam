﻿using System;

namespace Settings.Dto
{
    public class SurveyDto
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public User.Entity.User User { get; set; }

        public DateTime? DueDate { get; set; }
    }
}