﻿using Settings.Entity;

namespace Settings.Dto
{
    public class ModuleDto
    {
        public string Name { get; set; }
        public Product Product { get; set; }
    }
}