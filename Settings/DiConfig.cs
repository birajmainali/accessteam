﻿using Microsoft.Extensions.DependencyInjection;
using Settings.Entity;
using Settings.Repository;
using Settings.Repository.Interface;
using Settings.Services;
using Settings.Services.Interface;
using Settings.ValidationHelper;
using Settings.ValidationHelper.Interface;

namespace Settings
{
    public static class DiConfig
    {
        public static IServiceCollection UseSettings(this IServiceCollection service)
        {
            return service.AddScoped<IProductRepository, ProductRepository>()
                .AddScoped<IProductValidator, ProductValidator>()
                .AddScoped<IModuleServiceValidator, ModuleServiceValidator>()
                .AddScoped<ITaskSetupRepository, TaskSetupRepository>()
                .AddScoped<ITaskSetupService, TaskSetupService>()
                .AddScoped<IProductServices, ProductServices>()
                .AddScoped<ISurveyService, SurveyService>()
                .AddScoped<ISurveyRepository, SurveyRepository>()
                .AddScoped<ISurveyValidator, SurveyValidator>()
                .AddScoped<IModuleRepository, ModuleRepository>()
                .AddScoped<IModuleService, ModuleService>();
        }
    }
}