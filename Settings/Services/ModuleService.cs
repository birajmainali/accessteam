﻿using System.Threading.Tasks;
using System.Transactions;
using Settings.Dto;
using Settings.Entity;
using Settings.Repository.Interface;
using Settings.Services.Interface;
using Settings.ValidationHelper.Interface;

namespace Settings.Services
{
    public class ModuleService : IModuleService
    {
        private readonly IModuleRepository _moduleRepository;
        private readonly IModuleServiceValidator _serviceValidator;

        public ModuleService(IModuleRepository moduleRepository, IModuleServiceValidator serviceValidator)
        {
            _moduleRepository = moduleRepository;
            _serviceValidator = serviceValidator;
        }

        public async Task Create(ModuleDto dto)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            await _serviceValidator.CheckUniqueModuleName(dto.Name, dto.Product.Name);
            var module = new Module(dto.Name, dto.Product);
            await _moduleRepository.CreateAsync(module);
            await _moduleRepository.FlushAsync();
            tsc.Complete();
        }

        public async Task Remove(Module module)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            _moduleRepository.Remove(module);
            await _moduleRepository.FlushAsync();
            tsc.Complete();
        }

        public async Task CreateBulk(BulkModuleAddDto dto)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            foreach (var name in dto.Modules)
            {
                await Create(new ModuleDto
                {
                    Product = dto.Product,
                    Name = name
                });
            }

            tsc.Complete();
        }
    }
}