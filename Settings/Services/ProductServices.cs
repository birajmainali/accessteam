﻿using System;
using System.Threading.Tasks;
using System.Transactions;
using Settings.Dto;
using Settings.Entity;
using Settings.Repository.Interface;
using Settings.Services.Interface;
using Settings.ValidationHelper.Interface;

namespace Settings.Services
{
    public class ProductServices : IProductServices
    {
        private readonly IProductRepository _productRepository;
        private readonly IProductValidator _productValidator;

        public ProductServices(IProductRepository productRepository, IProductValidator productValidator)
        {
            _productRepository = productRepository;
            _productValidator = productValidator;
        }

        public async Task Create(ProductDto dto)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            await _productValidator.CheckDuplicatesProductName(dto.Name);
            var product = new Product
            {
                Name = dto.Name,
                Description = dto.Description
            };
            await _productRepository.CreateAsync(product);
            await _productRepository.FlushAsync();
            tsc.Complete();
        }

        public async Task Update(Product product, ProductDto dto)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            await _productValidator.CheckDuplicatesProductName(dto.Name, product.Id);
            product.Description = dto.Description;
            product.Name = dto.Name;
            product.ChangeDate = DateTime.Now;
            product.RecVersion++;
            product.RecAuditLog = $"{dto.Description}";
            _productRepository.Update(product);
            await _productRepository.FlushAsync();
            tsc.Complete();
        }

        public async Task Remove(Product product)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            await _productValidator.CheckProductIsInUse(product.Id);
            _productRepository.Remove(product);
            await _productRepository.FlushAsync();
            tsc.Complete();
        }
    }
}