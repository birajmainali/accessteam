﻿using System.Threading.Tasks;
using Settings.Dto;
using Settings.Entity;

namespace Settings.Services.Interface
{
    public interface IModuleService
    {
        Task Create(ModuleDto dto);
        Task Remove(Module module);
        Task CreateBulk(BulkModuleAddDto dto);
    }
}