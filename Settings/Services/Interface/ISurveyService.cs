﻿using System;
using System.Threading.Tasks;
using Settings.Dto;
using Settings.Entity;

namespace Settings.Services.Interface
{
    public interface ISurveyService
    {
        Task Create(SurveyDto dto);
        Task Update(Survey survey, SurveyDto dto);
        Task Remove(Survey survey);

        Task Publish(Survey survey, DateTime dueDate);

        Task Pause(Survey survey);
    }
}