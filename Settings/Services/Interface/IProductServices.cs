﻿using System.Threading.Tasks;
using Settings.Dto;
using Settings.Entity;

namespace Settings.Services.Interface
{
    public interface IProductServices
    {
        Task Create(ProductDto dto);
        Task Update(Product product, ProductDto dto);
        Task Remove(Product product);
    }
}