﻿using System.Threading.Tasks;
using Settings.Dto;
using Settings.Entity;

namespace Settings.Services.Interface
{
    public interface ITaskSetupService
    {
        Task Create(TaskSetupDto dto);
        Task Update(TaskSetup taskSetup, TaskSetupDto dto);
        Task Remove(TaskSetup taskSetup);
    }
}