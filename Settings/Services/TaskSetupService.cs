﻿using System.Threading.Tasks;
using System.Transactions;
using Settings.Dto;
using Settings.Entity;
using Settings.Repository.Interface;
using Settings.Services.Interface;

namespace Settings.Services
{
    public class TaskSetupService : ITaskSetupService
    {
        private readonly ITaskSetupRepository _taskSetupRepository;

        public TaskSetupService(ITaskSetupRepository taskSetupRepository)
        {
            _taskSetupRepository = taskSetupRepository;
        }

        public async Task Create(TaskSetupDto dto)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            var task = new TaskSetup(dto.Type, dto.Description, dto.SuccessPoint, dto.Module);
            await _taskSetupRepository.CreateAsync(task);
            await _taskSetupRepository.FlushAsync();
            tsc.Complete();
        }

        public async Task Update(TaskSetup taskSetup, TaskSetupDto dto)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            taskSetup.Update(dto.Type, dto.Description, dto.SuccessPoint, dto.Module);
            _taskSetupRepository.Update(taskSetup);
            await _taskSetupRepository.FlushAsync();
            tsc.Complete();
        }

        public async Task Remove(TaskSetup taskSetup)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            _taskSetupRepository.Remove(taskSetup);
            await _taskSetupRepository.FlushAsync();
            tsc.Complete();
        }
    }
}