﻿using System;
using System.Threading.Tasks;
using System.Transactions;
using Settings.Dto;
using Settings.Entity;
using Settings.Repository.Interface;
using Settings.Services.Interface;
using Settings.ValidationHelper.Interface;

namespace Settings.Services
{
    public class SurveyService : ISurveyService
    {
        private readonly ISurveyRepository _surveyRepository;
        private readonly ISurveyValidator _surveyValidator;

        public SurveyService(ISurveyRepository surveyRepository, ISurveyValidator surveyValidator)
        {
            _surveyRepository = surveyRepository;
            _surveyValidator = surveyValidator;
        }

        public async Task Create(SurveyDto dto)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            var survey = new Survey(dto.Title, dto.Description, dto.User);
            await _surveyRepository.CreateAsync(survey);
            await _surveyRepository.FlushAsync();
            tsc.Complete();
        }

        public async Task Update(Survey survey, SurveyDto dto)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            await _surveyValidator.CheckIfAlreadyPublish(survey.Id);
            survey.Update(dto.Title, dto.Description, dto.User);
            _surveyRepository.Update(survey);
            await _surveyRepository.FlushAsync();
            tsc.Complete();
        }

        public async Task Remove(Survey survey)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            await _surveyValidator.CheckIfAlreadyPublish(survey.Id);
            _surveyRepository.Remove(survey);
            await _surveyRepository.FlushAsync();
            tsc.Complete();
        }

        public async Task Publish(Survey survey, DateTime dueDate)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            await _surveyValidator.CheckIfAlreadyPublish(survey.Id);
            survey.DueDate = dueDate;
            survey.PublishDate = DateTime.Now;
            survey.IsPublish = true;
            _surveyRepository.Update(survey);
            await _surveyRepository.FlushAsync();
            tsc.Complete();
        }

        public async Task Pause(Survey survey)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            survey.IsPublish = false;
            survey.DueDate = null;
            survey.PublishDate = null;
            _surveyRepository.Update(survey);
            await _surveyRepository.FlushAsync();
            tsc.Complete();
        }
    }
}