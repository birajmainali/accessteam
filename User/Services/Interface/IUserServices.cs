﻿using System.Threading.Tasks;
using User.Dto;

namespace User.Services.Interface
{
    public interface IUserServices
    {
        Task CreateUser(UserDto dto);
        Task ApproveUser(Entity.User user);
        Task Remove(Entity.User user);
    }
}