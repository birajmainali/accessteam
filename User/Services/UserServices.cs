﻿using System;
using System.Threading.Tasks;
using System.Transactions;
using User.Dto;
using User.Provider.Interfaces;
using User.Repository.Interface;
using User.Services.Interface;

namespace User.Services
{
    public class UserServices : IUserServices
    {
        private readonly ICrypter _crypter;
        private readonly IUserRepository _userRepository;

        public UserServices(IUserRepository userRepository, ICrypter crypter)
        {
            _userRepository = userRepository;
            _crypter = crypter;
        }

        public async Task CreateUser(UserDto dto)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            var user = new Entity.User(dto.FirstName.Trim(), dto.LastName.Trim(), dto.DisplayName.Trim(),
                dto.Email.Trim(),
                _crypter.Crypt(dto.Password), dto.Address.Trim(), dto.Phone.Trim(), dto.Gender.Trim());
            await _userRepository.CreateAsync(user);
            await _userRepository.FlushAsync();
            tsc.Complete();
        }

        public async Task ApproveUser(Entity.User user)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            user.Approve(true, DateTime.Now);
            _userRepository.Update(user);
            await _userRepository.FlushAsync();
            tsc.Complete();
        }

        public async Task Remove(Entity.User user)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            _userRepository.Remove(user);
            await _userRepository.FlushAsync();
            tsc.Complete();
        }
    }
}