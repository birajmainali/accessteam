﻿using Microsoft.Extensions.DependencyInjection;
using User.Provider;
using User.Provider.Interfaces;
using User.Repository;
using User.Repository.Interface;
using User.Services;
using User.Services.Interface;

namespace User
{
    public static class DiConfig
    {
        public static IServiceCollection UseUser(this IServiceCollection service)
        {
            return service.AddScoped<IUserRepository, UserRepository>()
                .AddScoped<ICrypter, Crypter>()
                .AddScoped<IUserServices, UserServices>()
                .AddScoped<ICurrentUserProvider, CurrentUserProvider>();
        }
    }
}