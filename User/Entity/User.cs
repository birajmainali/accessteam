﻿using System;
using Base.Entity;

namespace User.Entity
{
    public class User : BaseEntity
    {
        protected User()
        {
        }

        public User(string firstName, string lastName, string username, string email, string password,
            string address, string phone, string gender)
        {
            Copy(firstName, lastName, username, email, password, address, phone, gender);
        }

        public string FirstName { get; protected set; }
        public string LastName { get; protected set; }
        public string DisplayName { get; protected set; }
        public string Gender { get; protected set; }
        public string Email { get; protected set; }
        public string Password { get; protected set; }
        public string Address { get; protected set; }
        public string Phone { get; protected set; }
        public DateTime? ApproveDate { get; protected set; }
        public bool IsApprove { get; protected set; }

        private void Copy(string firstname, string lastname, string username, string email, string password,
            string address, string phone, string gender)
        {
            FirstName = firstname;
            LastName = lastname;
            DisplayName = username;
            Email = email;
            Password = password;
            Address = address;
            Phone = phone;
            Gender = gender;
        }

        public void Approve(bool isApprove, DateTime approveDate)
        {
            IsApprove = isApprove;
            ApproveDate = approveDate;
        }
    }
}