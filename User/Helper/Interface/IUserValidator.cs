﻿using System.Threading.Tasks;

namespace User.Helper.Interface
{
    public interface IUserValidator
    {
        Task AuthorizedUser(string plainText, string cipherText);
    }
}