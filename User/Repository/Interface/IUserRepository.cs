﻿using Base.GenericRepository.Interface;

namespace User.Repository.Interface
{
    public interface IUserRepository : IGenericRepository<Entity.User>
    {
    }
}