﻿using Base.GenericRepository;
using Microsoft.EntityFrameworkCore;
using User.Repository.Interface;

namespace User.Repository
{
    public class UserRepository : GenericRepository<Entity.User>, IUserRepository
    {
        public UserRepository(DbContext context) : base(context)
        {
        }
    }
}