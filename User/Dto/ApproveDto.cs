﻿using System;

namespace User.Dto
{
    public class ApproveDto
    {
        public bool IsApprove { get; set; } = true;
        public DateTime ApproveDate { get; set; }
    }
}