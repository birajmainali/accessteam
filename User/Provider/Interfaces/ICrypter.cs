﻿namespace User.Provider.Interfaces
{
    public interface ICrypter
    {
        string Crypt(string plainText);
        bool Verify(string plainText, string cipherText);
    }
}