﻿using System.Threading.Tasks;

namespace User.Provider.Interfaces
{
    public interface ICurrentUserProvider
    {
        bool IsLoggedIn();
        Task<Entity.User> GetCurrentUser();
        long? GetCurrentUserId();
    }
}