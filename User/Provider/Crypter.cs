﻿using System;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using User.Provider.Interfaces;

namespace User.Provider
{
    public class Crypter : ICrypter
    {
        private const int SaltPosition = 0;
        private const int CipherPosition = 1;

        public string Crypt(string plainText)
        {
            var salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            var cipherTextBytes = getCipherTextBytes(plainText, salt);
            var cipherText = embedSaltInCipherText(cipherTextBytes, salt);
            return cipherText;
        }

        public bool Verify(string plainText, string cipherText)
        {
            var salt = ExtractSaltFromCipherText(cipherText);
            var expectedHashedBytes = ExtractHashBytesFromCipherText(cipherText);

            var actualHashedBytes = getCipherTextBytes(plainText, salt);
            return ByteArraysEqual(expectedHashedBytes, actualHashedBytes);
        }

        private byte[] getCipherTextBytes(string plainText, byte[] salt)
        {
            return KeyDerivation.Pbkdf2(
                plainText,
                salt,
                KeyDerivationPrf.HMACSHA1,
                10000,
                256 / 8);
        }

        private string[] GetSplittedCipherText(string hash)
        {
            return hash.Split(new[] {':'});
        }

        protected byte[] ExtractHashBytesFromCipherText(string hash)
        {
            var splittedHash = GetSplittedCipherText(hash);
            return Convert.FromBase64String(splittedHash[CipherPosition]);
        }

        private byte[] ExtractSaltFromCipherText(string hash)
        {
            var splittedHash = GetSplittedCipherText(hash);
            return Convert.FromBase64String(splittedHash[SaltPosition]);
        }

        public string embedSaltInCipherText(byte[] cipherTextBytes, byte[] salt)
        {
            return Convert.ToBase64String(salt) + ":" + Convert.ToBase64String(cipherTextBytes);
        }

        private static bool ByteArraysEqual(byte[] a, byte[] b)
        {
            if (a == null && b == null) return true;

            if (a == null || b == null || a.Length != b.Length) return false;

            var areSame = true;
            for (var i = 0; i < a.Length; i++) areSame &= a[i] == b[i];

            return areSame;
        }
    }
}