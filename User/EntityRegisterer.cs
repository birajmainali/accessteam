﻿using Base.Constants;
using Microsoft.EntityFrameworkCore;

namespace User
{
    public static class EntityRegisterer
    {
        public static ModelBuilder AddUser(this ModelBuilder builder)
        {
            builder.Entity<Entity.User>().ToTable("auth_users", Schema.User);
            return builder;
        }
    }
}