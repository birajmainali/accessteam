﻿using Base.Entity;

namespace Team.Entity
{
    public class Team : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}