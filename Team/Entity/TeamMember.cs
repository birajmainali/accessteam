using Base.Entity;

namespace Team.Entity
{
    public class TeamMember : BaseEntity
    {
        protected TeamMember()
        {
        }
        //  suiii

        public TeamMember(User.Entity.User user, Team team)
        {
            Copy(user, team);
        }

        public virtual User.Entity.User User { get; protected set; }
        public long UserId { get; set; }
        public virtual Team Team { get; protected set; }
        public long TeamId { get; set; }

        private void Copy(User.Entity.User user, Team team)
        {
            User = user;
            Team = team;
        }

        public void Update(User.Entity.User user, Team team)
        {
            Copy(user, team);
        }
    }
}
