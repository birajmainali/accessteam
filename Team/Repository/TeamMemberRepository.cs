﻿using Base.GenericRepository;
using Microsoft.EntityFrameworkCore;
using Team.Entity;
using Team.Repository.Interfaces;

namespace Team.Repository
{
    public class TeamMemberRepository : GenericRepository<TeamMember>, ITeamMemberRepository
    {
        public TeamMemberRepository(DbContext context) : base(context)
        {
        }
    }
}