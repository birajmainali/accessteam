﻿using System.Threading.Tasks;
using Base.GenericRepository.Interface;

namespace Team.Repository.Interfaces
{
    public interface ITeamRepository : IGenericRepository<Entity.Team>
    {
        Task<bool> IsTeamNameUsed(string name, long? excludingId = null);
    }
}