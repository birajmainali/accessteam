﻿using Base.GenericRepository.Interface;
using Team.Entity;

namespace Team.Repository.Interfaces
{
    public interface ITeamMemberRepository : IGenericRepository<TeamMember>
    {
    }
}