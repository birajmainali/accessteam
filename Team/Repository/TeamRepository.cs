﻿using System.Threading.Tasks;
using Base.GenericRepository;
using Microsoft.EntityFrameworkCore;
using Team.Repository.Interfaces;

namespace Team.Repository
{
    public class TeamRepository : GenericRepository<Entity.Team>, ITeamRepository
    {
        public TeamRepository(DbContext context) : base(context)
        {
        }

        public async Task<bool> IsTeamNameUsed(string name, long? excludingId = null)
        {
            return await CheckIfExistAsync(x =>
                (excludingId == null || x.Id == excludingId) && x.Name.ToLower() == name.ToLower());
        }
    }
}