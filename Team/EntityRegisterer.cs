﻿using Base.Constants;
using Microsoft.EntityFrameworkCore;
using Team.Entity;

namespace Team
{
    public static class EntityRegisterer
    {
        public static ModelBuilder AddTeam(this ModelBuilder builder)
        {
            builder.Entity<Entity.Team>().ToTable("team_setup", Schema.Team);
            builder.Entity<TeamMember>().ToTable("team_member", Schema.Team);
            return builder;
        }
    }
}