﻿using System.Collections.Generic;

namespace Team.Dto
{
    public record BulkTeamMemberAddDto(Entity.Team Team, List<User.Entity.User> Users);
}