﻿namespace Team.Dto
{
    public class TeamMemberDto
    {
        public virtual User.Entity.User User { get; set; }
        public virtual Entity.Team Team { get; set; }
    }
}