﻿namespace Team.Dto
{
    public class TeamDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}