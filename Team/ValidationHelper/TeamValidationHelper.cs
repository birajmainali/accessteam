﻿using System;
using System.Threading.Tasks;
using Team.Repository.Interfaces;
using Team.ValidationHelper.Interfaces;

namespace Team.ValidationHelper
{
    public class TeamValidationHelper : ITeamValidationHelper
    {
        private readonly ITeamRepository _teamRepository;

        public TeamValidationHelper(ITeamRepository teamRepository)
        {
            _teamRepository = teamRepository;
        }

        public async Task CheckDuplicateTeamName(string name, long? id = null)
        {
            if (await _teamRepository.IsTeamNameUsed(name, id)) throw new Exception("Name Already is in use");
        }
    }
}