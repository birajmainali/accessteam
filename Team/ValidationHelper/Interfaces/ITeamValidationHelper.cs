﻿using System.Threading.Tasks;

namespace Team.ValidationHelper.Interfaces
{
    public interface ITeamValidationHelper
    {
        Task CheckDuplicateTeamName(string name, long? id = null);
    }
}