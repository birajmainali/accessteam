﻿using Microsoft.Extensions.DependencyInjection;
using Team.Repository;
using Team.Repository.Interfaces;
using Team.Services;
using Team.Services.Interfaces;
using Team.ValidationHelper;
using Team.ValidationHelper.Interfaces;

namespace Team
{
    public static class DiConfig
    {
        public static IServiceCollection UseTeam(this IServiceCollection service)
        {
            return service.AddScoped<ITeamRepository, TeamRepository>()
                .AddScoped<ITeamServices, TeamServices>()
                .AddScoped<ITeamValidationHelper, TeamValidationHelper>()
                .AddScoped<ITeamMemberService, TeamMemberService>()
                .AddScoped<ITeamMemberRepository, TeamMemberRepository>();
        }
    }
}