﻿using System;
using System.Threading.Tasks;
using System.Transactions;
using Team.Dto;
using Team.Repository.Interfaces;
using Team.Services.Interfaces;
using Team.ValidationHelper.Interfaces;
using TeamEntity = Team.Entity.Team;

namespace Team.Services
{
    public class TeamServices : ITeamServices
    {
        private readonly ITeamRepository _teamRepository;
        private readonly ITeamValidationHelper _validationHelper;

        public TeamServices(ITeamRepository teamRepository, ITeamValidationHelper validationHelper)
        {
            _teamRepository = teamRepository;
            _validationHelper = validationHelper;
        }

        public async Task Create(TeamDto dto)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            await _validationHelper.CheckDuplicateTeamName(dto.Name);
            var team = new TeamEntity
            {
                Name = dto.Name,
                Description = dto.Description
            };
            await _teamRepository.CreateAsync(team);
            await _teamRepository.FlushAsync();
            tsc.Complete();
        }

        public async Task Update(TeamEntity team, TeamDto dto)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            await _validationHelper.CheckDuplicateTeamName(dto.Name, team.Id);
            team.Name = dto.Name;
            team.Description = dto.Description;
            team.ChangeDate = DateTime.Now;
            _teamRepository.Update(team);
            await _teamRepository.FlushAsync();
            tsc.Complete();
        }

        public async Task Remove(TeamEntity team)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            _teamRepository.Remove(team);
            await _teamRepository.FlushAsync();
            tsc.Complete();
        }
    }
}