﻿using System.Threading.Tasks;
using Team.Dto;

namespace Team.Services.Interfaces
{
    public interface ITeamServices
    {
        Task Create(TeamDto dto);
        Task Update(Entity.Team team, TeamDto dto);
        Task Remove(Entity.Team team);
    }
}