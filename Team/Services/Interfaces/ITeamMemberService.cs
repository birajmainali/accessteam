﻿using System.Threading.Tasks;
using Team.Dto;
using Team.Entity;

namespace Team.Services.Interfaces
{
    public interface ITeamMemberService
    {
        Task Create(TeamMemberDto dto);
        Task Update(TeamMember member, TeamMemberDto dto);
        Task Remove(TeamMember member);
        Task CreateBulk(BulkTeamMemberAddDto dto);
    }
}