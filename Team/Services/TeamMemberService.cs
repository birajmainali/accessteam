﻿using System.Threading.Tasks;
using System.Transactions;
using Team.Dto;
using Team.Entity;
using Team.Repository.Interfaces;
using Team.Services.Interfaces;

namespace Team.Services
{
    public class TeamMemberService : ITeamMemberService
    {
        private readonly ITeamMemberRepository _teamMemberRepository;

        public TeamMemberService(ITeamMemberRepository teamMemberRepository)
        {
            _teamMemberRepository = teamMemberRepository;
        }

        public async Task Create(TeamMemberDto dto)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            var member = new TeamMember(dto.User, dto.Team);
            await _teamMemberRepository.CreateAsync(member);
            await _teamMemberRepository.FlushAsync();
            tsc.Complete();
        }

        public async Task Update(TeamMember member, TeamMemberDto dto)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            member.Update(dto.User, dto.Team);
            _teamMemberRepository.Update(member);
            await _teamMemberRepository.FlushAsync();
            tsc.Complete();
        }

        public async Task Remove(TeamMember member)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            _teamMemberRepository.Remove(member);
            await _teamMemberRepository.FlushAsync();
            tsc.Complete();
        }

        public async Task CreateBulk(BulkTeamMemberAddDto dto)
        {
            using var tsc = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            foreach (var user in dto.Users)
                await Create(new TeamMemberDto
                {
                    Team = dto.Team,
                    User = user
                });
            tsc.Complete();
        }
    }
}