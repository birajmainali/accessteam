﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AccessTeamWeb.Manager.Interfaces;
using Base.Constants;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using User.Provider.Interfaces;
using User.Repository.Interface;

namespace AccessTeamWeb.Manager
{
    public class AuthManager : IAuthManager
    {
        private readonly ICrypter _crypter;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IUserRepository _userRepo;

        public AuthManager(IHttpContextAccessor httpContextAccessor, IUserRepository userRepo, ICrypter crypter)
        {
            _httpContextAccessor = httpContextAccessor;
            _userRepo = userRepo;
            _crypter = crypter;
        }

        public async Task<AuthResult> Login(string identity, string password)
        {
            var user = await _userRepo.GetItemAsync(x => x.Email.ToLower() == identity.ToLower().Trim());
            var result = new AuthResult();
            if (user == null)
            {
                result.Success = false;
                result.Errors.Add("Invalid identity. User not found");
                return result;
            }

            if (user.RecStatus == Status.RecD)
                result.Errors.Add("Deleted User, Unable to login please Request for new");

            if (!_crypter.Verify(password, user.Password))
            {
                result.Success = false;
                result.Errors.Add("Invalid password");
                return result;
            }

            if (!user.IsApprove)
            {
                result.Success = false;
                result.Errors.Add("User not approved");
                return result;
            }

            var httpContext = _httpContextAccessor.HttpContext;
            var claims = new List<Claim>
            {
                new("Id", user.Id.ToString())
            };
            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            await httpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity));
            result.Success = true;
            return result;
        }
    }

    public class AuthResult
    {
        public List<string> Errors = new();
        public bool Success;
    }
}