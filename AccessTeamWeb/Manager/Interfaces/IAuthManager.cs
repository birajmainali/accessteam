﻿using System.Threading.Tasks;

namespace AccessTeamWeb.Manager.Interfaces
{
    public interface IAuthManager
    {
        Task<AuthResult> Login(string identity, string password);
    }
}