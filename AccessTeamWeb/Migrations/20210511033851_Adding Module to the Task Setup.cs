﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AccessTeamWeb.Migrations
{
    public partial class AddingModuletotheTaskSetup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "SuccessPoint",
                schema: "General",
                table: "task_setup",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric");

            migrationBuilder.AddColumn<long>(
                name: "ModuleId",
                schema: "General",
                table: "task_setup",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_task_setup_ModuleId",
                schema: "General",
                table: "task_setup",
                column: "ModuleId");

            migrationBuilder.AddForeignKey(
                name: "FK_task_setup_module_setup_ModuleId",
                schema: "General",
                table: "task_setup",
                column: "ModuleId",
                principalSchema: "General",
                principalTable: "module_setup",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_task_setup_module_setup_ModuleId",
                schema: "General",
                table: "task_setup");

            migrationBuilder.DropIndex(
                name: "IX_task_setup_ModuleId",
                schema: "General",
                table: "task_setup");

            migrationBuilder.DropColumn(
                name: "ModuleId",
                schema: "General",
                table: "task_setup");

            migrationBuilder.AlterColumn<decimal>(
                name: "SuccessPoint",
                schema: "General",
                table: "task_setup",
                type: "numeric",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint");
        }
    }
}
