﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AccessTeamWeb.Migrations
{
    public partial class removingproductfromTasksetup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_task_setup_product_setup_ProductId",
                schema: "General",
                table: "task_setup");

            migrationBuilder.DropIndex(
                name: "IX_task_setup_ProductId",
                schema: "General",
                table: "task_setup");

            migrationBuilder.DropColumn(
                name: "ProductId",
                schema: "General",
                table: "task_setup");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ProductId",
                schema: "General",
                table: "task_setup",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_task_setup_ProductId",
                schema: "General",
                table: "task_setup",
                column: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_task_setup_product_setup_ProductId",
                schema: "General",
                table: "task_setup",
                column: "ProductId",
                principalSchema: "General",
                principalTable: "product_setup",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
