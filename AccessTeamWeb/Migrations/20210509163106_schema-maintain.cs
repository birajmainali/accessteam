﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace AccessTeamWeb.Migrations
{
    public partial class schemamaintain : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Module_Product_ProductId",
                schema: "Settings",
                table: "Module");

            migrationBuilder.DropForeignKey(
                name: "FK_Survey_setup_User_info_UserId1",
                schema: "Settings",
                table: "Survey_setup");

            migrationBuilder.DropForeignKey(
                name: "FK_team_member_User_info_UserId",
                schema: "Team",
                table: "team_member");

            migrationBuilder.DropForeignKey(
                name: "FK_Work_info_client_info_ClientId",
                schema: "Work",
                table: "Work_info");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Survey_setup",
                schema: "Settings",
                table: "Survey_setup");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Work_info",
                schema: "Work",
                table: "Work_info");

            migrationBuilder.DropPrimaryKey(
                name: "PK_User_info",
                schema: "User",
                table: "User_info");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Product",
                schema: "Settings",
                table: "Product");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Module",
                schema: "Settings",
                table: "Module");

            migrationBuilder.EnsureSchema(
                name: "General");

            migrationBuilder.RenameTable(
                name: "Survey_setup",
                schema: "Settings",
                newName: "survey_setup",
                newSchema: "General");

            migrationBuilder.RenameTable(
                name: "Work_info",
                schema: "Work",
                newName: "works",
                newSchema: "Work");

            migrationBuilder.RenameTable(
                name: "User_info",
                schema: "User",
                newName: "auth_users",
                newSchema: "User");

            migrationBuilder.RenameTable(
                name: "Product",
                schema: "Settings",
                newName: "product_setup",
                newSchema: "General");

            migrationBuilder.RenameTable(
                name: "Module",
                schema: "Settings",
                newName: "module_setup",
                newSchema: "General");

            migrationBuilder.RenameIndex(
                name: "IX_Survey_setup_UserId1",
                schema: "General",
                table: "survey_setup",
                newName: "IX_survey_setup_UserId1");

            migrationBuilder.RenameIndex(
                name: "IX_Work_info_ClientId",
                schema: "Work",
                table: "works",
                newName: "IX_works_ClientId");

            migrationBuilder.RenameIndex(
                name: "IX_Module_ProductId",
                schema: "General",
                table: "module_setup",
                newName: "IX_module_setup_ProductId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_survey_setup",
                schema: "General",
                table: "survey_setup",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_works",
                schema: "Work",
                table: "works",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_auth_users",
                schema: "User",
                table: "auth_users",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_product_setup",
                schema: "General",
                table: "product_setup",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_module_setup",
                schema: "General",
                table: "module_setup",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "task_setup",
                schema: "General",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Type = table.Column<string>(type: "text", nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    SuccessPoint = table.Column<decimal>(type: "numeric", nullable: false),
                    ProductId = table.Column<long>(type: "bigint", nullable: false),
                    RecDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    ChangeDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    RecAuditLog = table.Column<string>(type: "text", nullable: true),
                    RecVersion = table.Column<long>(type: "bigint", nullable: false),
                    Status = table.Column<string>(type: "text", nullable: false),
                    RecStatus = table.Column<char>(type: "character(1)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_task_setup", x => x.Id);
                    table.ForeignKey(
                        name: "FK_task_setup_product_setup_ProductId",
                        column: x => x.ProductId,
                        principalSchema: "General",
                        principalTable: "product_setup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_task_setup_ProductId",
                schema: "General",
                table: "task_setup",
                column: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_module_setup_product_setup_ProductId",
                schema: "General",
                table: "module_setup",
                column: "ProductId",
                principalSchema: "General",
                principalTable: "product_setup",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_survey_setup_auth_users_UserId1",
                schema: "General",
                table: "survey_setup",
                column: "UserId1",
                principalSchema: "User",
                principalTable: "auth_users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_team_member_auth_users_UserId",
                schema: "Team",
                table: "team_member",
                column: "UserId",
                principalSchema: "User",
                principalTable: "auth_users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_works_client_info_ClientId",
                schema: "Work",
                table: "works",
                column: "ClientId",
                principalSchema: "Client",
                principalTable: "client_info",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_module_setup_product_setup_ProductId",
                schema: "General",
                table: "module_setup");

            migrationBuilder.DropForeignKey(
                name: "FK_survey_setup_auth_users_UserId1",
                schema: "General",
                table: "survey_setup");

            migrationBuilder.DropForeignKey(
                name: "FK_team_member_auth_users_UserId",
                schema: "Team",
                table: "team_member");

            migrationBuilder.DropForeignKey(
                name: "FK_works_client_info_ClientId",
                schema: "Work",
                table: "works");

            migrationBuilder.DropTable(
                name: "task_setup",
                schema: "General");

            migrationBuilder.DropPrimaryKey(
                name: "PK_survey_setup",
                schema: "General",
                table: "survey_setup");

            migrationBuilder.DropPrimaryKey(
                name: "PK_works",
                schema: "Work",
                table: "works");

            migrationBuilder.DropPrimaryKey(
                name: "PK_product_setup",
                schema: "General",
                table: "product_setup");

            migrationBuilder.DropPrimaryKey(
                name: "PK_module_setup",
                schema: "General",
                table: "module_setup");

            migrationBuilder.DropPrimaryKey(
                name: "PK_auth_users",
                schema: "User",
                table: "auth_users");

            migrationBuilder.EnsureSchema(
                name: "Settings");

            migrationBuilder.RenameTable(
                name: "survey_setup",
                schema: "General",
                newName: "Survey_setup",
                newSchema: "Settings");

            migrationBuilder.RenameTable(
                name: "works",
                schema: "Work",
                newName: "Work_info",
                newSchema: "Work");

            migrationBuilder.RenameTable(
                name: "product_setup",
                schema: "General",
                newName: "Product",
                newSchema: "Settings");

            migrationBuilder.RenameTable(
                name: "module_setup",
                schema: "General",
                newName: "Module",
                newSchema: "Settings");

            migrationBuilder.RenameTable(
                name: "auth_users",
                schema: "User",
                newName: "User_info",
                newSchema: "User");

            migrationBuilder.RenameIndex(
                name: "IX_survey_setup_UserId1",
                schema: "Settings",
                table: "Survey_setup",
                newName: "IX_Survey_setup_UserId1");

            migrationBuilder.RenameIndex(
                name: "IX_works_ClientId",
                schema: "Work",
                table: "Work_info",
                newName: "IX_Work_info_ClientId");

            migrationBuilder.RenameIndex(
                name: "IX_module_setup_ProductId",
                schema: "Settings",
                table: "Module",
                newName: "IX_Module_ProductId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Survey_setup",
                schema: "Settings",
                table: "Survey_setup",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Work_info",
                schema: "Work",
                table: "Work_info",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Product",
                schema: "Settings",
                table: "Product",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Module",
                schema: "Settings",
                table: "Module",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_User_info",
                schema: "User",
                table: "User_info",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Module_Product_ProductId",
                schema: "Settings",
                table: "Module",
                column: "ProductId",
                principalSchema: "Settings",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Survey_setup_User_info_UserId1",
                schema: "Settings",
                table: "Survey_setup",
                column: "UserId1",
                principalSchema: "User",
                principalTable: "User_info",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_team_member_User_info_UserId",
                schema: "Team",
                table: "team_member",
                column: "UserId",
                principalSchema: "User",
                principalTable: "User_info",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Work_info_client_info_ClientId",
                schema: "Work",
                table: "Work_info",
                column: "ClientId",
                principalSchema: "Client",
                principalTable: "client_info",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
