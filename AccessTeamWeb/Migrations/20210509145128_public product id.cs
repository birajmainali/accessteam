﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AccessTeamWeb.Migrations
{
    public partial class publicproductid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Module_Product_ProductId1",
                schema: "Settings",
                table: "Module");

            migrationBuilder.DropIndex(
                name: "IX_Module_ProductId1",
                schema: "Settings",
                table: "Module");

            migrationBuilder.DropColumn(
                name: "ProductId1",
                schema: "Settings",
                table: "Module");

            migrationBuilder.AddColumn<long>(
                name: "ProductId",
                schema: "Settings",
                table: "Module",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_Module_ProductId",
                schema: "Settings",
                table: "Module",
                column: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_Module_Product_ProductId",
                schema: "Settings",
                table: "Module",
                column: "ProductId",
                principalSchema: "Settings",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Module_Product_ProductId",
                schema: "Settings",
                table: "Module");

            migrationBuilder.DropIndex(
                name: "IX_Module_ProductId",
                schema: "Settings",
                table: "Module");

            migrationBuilder.DropColumn(
                name: "ProductId",
                schema: "Settings",
                table: "Module");

            migrationBuilder.AddColumn<long>(
                name: "ProductId1",
                schema: "Settings",
                table: "Module",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Module_ProductId1",
                schema: "Settings",
                table: "Module",
                column: "ProductId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Module_Product_ProductId1",
                schema: "Settings",
                table: "Module",
                column: "ProductId1",
                principalSchema: "Settings",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
