﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AccessTeamWeb.Controllers.ViewModel;
using AccessTeamWeb.Manager.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using User.Dto;
using User.Services.Interface;

namespace AccessTeamWeb.Controllers
{
    [AllowAnonymous]
    public class LoginController : Controller
    {
        private readonly IAuthManager _authManager;
        private readonly IToastNotification _notification;
        private readonly IUserServices _userServices;


        public LoginController(
            IUserServices userServices, IAuthManager authManager, IToastNotification notification)
        {
            _userServices = userServices;
            _authManager = authManager;
            _notification = notification;
        }


        public IActionResult Index()
        {
            try
            {
                return View(new LoginVm());
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return View(new LoginVm());
            }
        }

        [HttpPost]
        public async Task<IActionResult> Index(LoginVm vm)
        {
            try
            {
                var result = await _authManager.Login(vm.Email, vm.Password);
                if (result.Success) return RedirectToAction("Index", "Home");

                ModelState.AddModelError(nameof(vm.Email), result.Errors.FirstOrDefault());
                vm.Password = "";
                _notification.AddErrorToastMessage("Identity information is not matching");
                return View(vm);
            }
            catch (Exception e)
            {
                _notification.AddWarningToastMessage(e.Message);
                return View();
            }
        }

        [HttpGet]
        public IActionResult Registration()
        {
            try
            {
                return View(new UserVm());
            }
            catch (Exception ex)
            {
                _notification.AddErrorToastMessage(ex.Message);
                return View(new UserVm());
            }
        }

        [HttpPost]
        public async Task<IActionResult> Registration(UserVm vm)
        {
            try
            {
                var dto = new UserDto
                {
                    DisplayName = vm.DisplayName,
                    FirstName = vm.FirstName,
                    LastName = vm.LastName,
                    Address = vm.Address,
                    Email = vm.Email,
                    Phone = vm.Phone,
                    Password = vm.Password,
                    Gender = vm.Gender
                };
                await _userServices.CreateUser(dto);
                _notification.AddSuccessToastMessage("your first login Request has been sent to the administrator");
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return View();
            }
        }
    }
}