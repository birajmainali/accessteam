﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AccessTeamWeb.Controllers.ViewModel
{
    public class UserVm
    {
        [Required] [DisplayName("First Name")] public string FirstName { get; set; }
        [Required] [DisplayName("Last Name")] public string LastName { get; set; }

        [Required]
        [DisplayName("Display Name")]
        public string DisplayName { get; set; }

        [Required] [DisplayName("Email")] public string Email { get; set; }
        [Required] [DisplayName("Gender")] public string Gender { get; set; }
        [Required] [DisplayName("Password")] public string Password { get; set; }
        [Required] [DisplayName("Address")] public string Address { get; set; }
        [Required] [DisplayName("Phone")] public string Phone { get; set; }
    }
}