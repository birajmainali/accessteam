﻿namespace AccessTeamWeb.Controllers.ViewModel
{
    public class LoginVm
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}