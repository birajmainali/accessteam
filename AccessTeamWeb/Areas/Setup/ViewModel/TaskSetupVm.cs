﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Settings.Entity;

namespace AccessTeamWeb.Areas.Setup.ViewModel
{
    public class TaskSetupVm
    {
        [DisplayName("Task Type")] [Required] public string Type { get; set; }

        [DisplayName("Description")] public string? Description { get; set; }

        [Description("Success Point")] public long Point { get; set; }

        [DisplayName("Module")] [Required] public long ModuleId { get; set; }   

        public List<Module> Modules { get; set; }

        public SelectList SelectModule()
        {
            return new SelectList(Modules, nameof(Module.Id), nameof(Module.Name));
        }
    }
}