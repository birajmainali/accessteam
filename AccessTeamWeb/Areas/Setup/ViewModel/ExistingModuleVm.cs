﻿using System.Collections.Generic;
using Settings.Entity;

namespace AccessTeamWeb.Areas.Setup.ViewModel
{
    public class ExistingModuleVm
    {
        public Product Product { get; set; }
        public List<Module> Modules { get; set; }
    }
}