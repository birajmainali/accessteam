﻿using System;

namespace AccessTeamWeb.Areas.Setup.ViewModel
{
    public class SurveyVm
    {
        public string Title { get; set; }
        public string? Description { get; set; }
        public DateTime? DueDate { get; set; }
    }
}