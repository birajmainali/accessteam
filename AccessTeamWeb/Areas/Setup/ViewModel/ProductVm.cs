﻿using System.ComponentModel;

namespace AccessTeamWeb.Areas.Setup.ViewModel
{
    public class ProductVm
    {
        [DisplayName("Product Name")] public string Name { get; set; }
        [DisplayName("Description")] public string Description { get; set; }
    }
}