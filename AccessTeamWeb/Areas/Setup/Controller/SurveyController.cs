﻿using System;
using System.Threading.Tasks;
using AccessTeamWeb.Areas.Setup.ViewModel;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using Settings.Dto;
using Settings.Repository.Interface;
using Settings.Services.Interface;
using User.Provider.Interfaces;
using User.Repository.Interface;

namespace AccessTeamWeb.Areas.Setup.Controller
{
    [Area("Setup")]
    public class SurveyController : Microsoft.AspNetCore.Mvc.Controller
    {
        private readonly ICurrentUserProvider _currentUserProvider;
        private readonly IToastNotification _notification;
        private readonly ISurveyRepository _surveyRepository;
        private readonly ISurveyService _surveyService;
        private readonly IUserRepository _userRepository;

        public SurveyController(IUserRepository userRepository, ISurveyRepository surveyRepository,
            ISurveyService surveyService, ICurrentUserProvider currentUserProvider, IToastNotification notification)
        {
            _userRepository = userRepository;
            _surveyRepository = surveyRepository;
            _surveyService = surveyService;
            _currentUserProvider = currentUserProvider;
            _notification = notification;
        }

        public async Task<IActionResult> Index()
        {
            var setup = await _surveyRepository.GetAllAsync();
            return View(setup);
        }

        public IActionResult New()
        {
            // var userId = _currentUserProvider.GetCurrentUserId().Value;
            return View(new SurveyVm());
        }

        [HttpPost]
        public async Task<IActionResult> New(SurveyVm vm)
        {
            try
            {
                // var userId = _currentUserProvider.GetCurrentUserId().Value;
                if (!ModelState.IsValid) return View(vm);

                var dto = new SurveyDto
                {
                    Title = vm.Title,
                    Description = vm.Description,
                    // User = await _userRepository.FindOrThrowAsync(userId)
                    User = await _currentUserProvider.GetCurrentUser()
                };
                await _surveyService.Create(dto);
                _notification.AddSuccessToastMessage($"{dto.Title} is Created");
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return View(vm);
            }
        }

        public async Task<IActionResult> Edit(long id)
        {
            try
            {
                // var userId = _currentUserProvider.GetCurrentUserId()!.Value;
                var survey = await _surveyRepository.FindOrThrowAsync(id);
                var vm = new SurveyVm
                {
                    Title = survey.Title,
                    Description = survey.Description,
                };
                return View(vm);
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return RedirectToAction(nameof(Index));
            }
        }

        [HttpPost]
        public async Task<IActionResult> Edit(long id, SurveyVm vm)
        {
            try
            {
                var survey = await _surveyRepository.FindOrThrowAsync(id);
                // var userId = _currentUserProvider.GetCurrentUserId().Value;
                if (!ModelState.IsValid) return View(vm);

                var dto = new SurveyDto
                {
                    Title = vm.Title,
                    Description = vm.Description,
                    // User = await _userRepository.FindOrThrowAsync(userId)
                    User = await _currentUserProvider.GetCurrentUser()
                };

                await _surveyService.Update(survey, dto);
                _notification.AddSuccessToastMessage($"{dto.Title} has edited");
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return RedirectToAction(nameof(Index));
            }
        }

        public async Task<IActionResult> Publish(long id, DateTime dueDate)
        {
            try
            {
                var survey = await _surveyRepository.FindOrThrowAsync(id);
                await _surveyService.Publish(survey, dueDate);
                _notification.AddSuccessToastMessage($"{survey.Title} has been Publish..");
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return RedirectToAction("Index");
            }
        }

        public async Task<IActionResult> Pause(long id)
        {
            try
            {
                var survey = await _surveyRepository.FindOrThrowAsync(id);
                await _surveyService.Pause(survey);
                _notification.AddSuccessToastMessage($"{survey.Title} has been Pause");
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return RedirectToAction("Index");
            }
        }


        public async Task<IActionResult> Remove(long id)
        {
            try
            {
                var survey = await _surveyRepository.FindOrThrowAsync(id);
                await _surveyService.Remove(survey);
                _notification.AddSuccessToastMessage($"{survey.Title} Deleted");
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return RedirectToAction(nameof(Index));
            }
        }
    }
}