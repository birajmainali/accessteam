﻿using System;
using System.Threading.Tasks;
using AccessTeamWeb.Areas.Setup.ViewModel;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using Settings.Repository.Interface;
using Settings.Services.Interface;

namespace AccessTeamWeb.Areas.Setup.Controller
{
    [Area("Setup")]
    public class ModuleController : Microsoft.AspNetCore.Mvc.Controller
    {
        private readonly IToastNotification _notification;
        private readonly IModuleRepository _moduleRepository;
        private readonly IModuleService _moduleService;
        private readonly IProductRepository _productRepository;

        public ModuleController(IToastNotification notification, IModuleRepository moduleRepository,
            IModuleService moduleService, IProductRepository productRepository)
        {
            _notification = notification;
            _moduleRepository = moduleRepository;
            _moduleService = moduleService;
            _productRepository = productRepository;
        }

        public async Task<IActionResult> Index()
        {
            var module = await _moduleRepository.GetAllAsync();
            return View(module);
        }

        public async Task<IActionResult> AddModule(long id)
        {
            try
            {
                return View(new ExistingModuleVm()
                {
                    Product = await _productRepository.FindOrThrowAsync(id),
                    Modules = await _moduleRepository.GetAllAsync()
                });
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return RedirectToAction("Index", "Product");
            }
        }

        public async Task<IActionResult> Remove(long id)
        {
            try
            {
                var module = await _moduleRepository.FindOrThrowAsync(id);
                await _moduleService.Remove(module);
                _notification.AddSuccessToastMessage("Deleted");
                return RedirectToAction(nameof(AddModule), new {id = module.ProductId});
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return RedirectToAction("Index");
            }
        }
    }
}