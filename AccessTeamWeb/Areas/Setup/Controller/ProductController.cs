﻿using System;
using System.Threading.Tasks;
using AccessTeamWeb.Areas.Setup.ViewModel;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using Settings.Dto;
using Settings.Repository.Interface;
using Settings.Services.Interface;

namespace AccessTeamWeb.Areas.Setup.Controller
{
    [Area("Setup")]
    public class ProductController : Microsoft.AspNetCore.Mvc.Controller
    {
        private readonly IToastNotification _notification;
        private readonly IProductRepository _productRepository;
        private readonly IProductServices _productServices;

        public ProductController(IToastNotification notification, IProductRepository productRepository,
            IProductServices productServices)
        {
            _notification = notification;
            _productRepository = productRepository;
            _productServices = productServices;
        }

        public async Task<IActionResult> Index()
        {
            var products = await _productRepository.GetAllAsync();
            return View(products);
        }

        public IActionResult New()
        {
            return View(new ProductVm());
        }

        [HttpPost]
        public async Task<IActionResult> New(ProductVm vm)
        {
            try
            {
                var dto = new ProductDto
                {
                    Description = vm.Description,
                    Name = vm.Name
                };
                await _productServices.Create(dto);
                _notification.AddSuccessToastMessage($"{dto.Name} has been created");
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return View(vm);
            }
        }

        public async Task<IActionResult> Edit(long id)
        {
            try
            {
                var product = await _productRepository.FindOrThrowAsync(id);
                var vm = new ProductVm
                {
                    Description = product.Description,
                    Name = product.Name
                };
                return View(vm);
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return RedirectToAction(nameof(Index));
            }
        }

        [HttpPost]
        public async Task<IActionResult> Edit(long id, ProductVm vm)
        {
            try
            {
                if (!ModelState.IsValid) return View(vm);
                {
                }
                var product = await _productRepository.FindOrThrowAsync(id);
                var dto = new ProductDto
                {
                    Description = vm.Description,
                    Name = vm.Name
                };
                await _productServices.Update(product, dto);
                _notification.AddSuccessToastMessage($"{dto.Name} is created");
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return RedirectToAction(nameof(Index));
            }
        }

        public async Task<IActionResult> Remove(long id)
        {
            try
            {
                var product = await _productRepository.FindOrThrowAsync(id);
                await _productServices.Remove(product);
                _notification.AddSuccessToastMessage($"{product.Name} has been deleted");
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return RedirectToAction(nameof(Index));
            }
        }
    }
}