﻿using System;
using System.Threading.Tasks;
using AccessTeamWeb.Areas.Setup.ViewModel;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using Settings.Dto;
using Settings.Repository.Interface;
using Settings.Services.Interface;

namespace AccessTeamWeb.Areas.Setup.Controller
{
    [Area("Setup")]
    public class TaskSetupController : Microsoft.AspNetCore.Mvc.Controller
    {
        private readonly IToastNotification _notification;
        private readonly ITaskSetupService _taskSetupService;
        private readonly ITaskSetupRepository _taskSetupRepository;
        private readonly IModuleRepository _moduleRepository;

        public TaskSetupController(IToastNotification notification,
            ITaskSetupService taskSetupService, ITaskSetupRepository taskSetupRepository,
            IModuleRepository moduleRepository)
        {
            _notification = notification;
            _taskSetupService = taskSetupService;
            _taskSetupRepository = taskSetupRepository;
            _moduleRepository = moduleRepository;
        }

        public async Task<IActionResult> Index()
        {
            var setups = await _taskSetupRepository.GetAllAsync();
            return View(setups);
        }

        public async Task<IActionResult> New()
        {
            return View(new TaskSetupVm
            {
                Modules = await _moduleRepository.GetAllAsync(),
            });
        }

        [HttpPost]
        public async Task<IActionResult> New(TaskSetupVm vm)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(vm);
                }

                var dto = new TaskSetupDto()
                {
                    Type = vm.Type,
                    Description = vm.Description,
                    SuccessPoint = vm.Point,
                    Module = await _moduleRepository.FindOrThrowAsync(vm.ModuleId),
                };
                await _taskSetupService.Create(dto);
                _notification.AddSuccessToastMessage($"{dto.Type} has been created");
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return View(vm);
            }
        }

        public async Task<IActionResult> Edit(long id)
        {
            try
            {
                var setup = await _taskSetupRepository.FindOrThrowAsync(id);
                var vm = new TaskSetupVm()
                {
                    Type = setup.Type,
                    Description = setup.Description,
                    Point = setup.SuccessPoint,
                    ModuleId = setup.ModuleId,
                    Modules = await _moduleRepository.GetAllAsync()
                };
                return View(vm);
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return RedirectToAction(nameof(Index));
            }
        }

        [HttpPost]
        public async Task<IActionResult> Edit(long id, TaskSetupVm vm)
        {
            try
            {
                var setup = await _taskSetupRepository.FindOrThrowAsync(id);
                if (!ModelState.IsValid) return View(vm);
                var dto = new TaskSetupDto()
                {
                    Type = vm.Type.Trim(),
                    Description = vm.Description.Trim(),
                    SuccessPoint = vm.Point,
                    Module = await _moduleRepository.FindOrThrowAsync(vm.ModuleId),
                };
                await _taskSetupService.Update(setup, dto);
                _notification.AddSuccessToastMessage($"{setup.Type} has been updated");
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return RedirectToAction("Index");
            }
        }

        public async Task<IActionResult> Remove(long id)
        {
            try
            {
                var setup = await _taskSetupRepository.FindOrThrowAsync(id);
                await _taskSetupService.Remove(setup);
                _notification.AddSuccessToastMessage($"{setup.Type} has been removed");
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return RedirectToAction("Index");
            }
        }
    }
}