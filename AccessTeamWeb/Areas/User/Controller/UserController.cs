﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using User.Provider.Interfaces;
using User.Repository.Interface;
using User.Services.Interface;

namespace AccessTeamWeb.Areas.User.Controller
{
    [Area("User")]
    public class UserController : Microsoft.AspNetCore.Mvc.Controller
    {
        private readonly IToastNotification _notification;
        private readonly IUserRepository _userRepository;
        private readonly IUserServices _userServices;
        private readonly ICurrentUserProvider _currentUserProvider;


        public UserController(IUserRepository userRepository, IToastNotification notification,
            IUserServices userServices, ICurrentUserProvider currentUserProvider)
        {
            _userRepository = userRepository;
            _notification = notification;
            _userServices = userServices;
            _currentUserProvider = currentUserProvider;
        }

        public async Task<IActionResult> Index()
        {
            var users = await _userRepository.GetAllAsync(x => x.Id != _currentUserProvider.GetCurrentUserId().Value);
            return View(users);
        }

        public async Task<IActionResult> Approve(long id)
        {
            try
            {
                var user = await _userRepository.FindOrThrowAsync(id);
                await _userServices.ApproveUser(user);
                _notification.AddSuccessToastMessage($"{user.DisplayName} has been Approved");
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return RedirectToAction(nameof(Index));
            }
        }

        public async Task<IActionResult> Remove(long id)
        {
            try
            {
                var user = await _userRepository.FindOrThrowAsync(id);
                await _userServices.Remove(user);
                _notification.AddSuccessToastMessage($"{user.DisplayName} has been removed");
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return RedirectToAction(nameof(Index));
            }
        }
    }
}