﻿using Microsoft.AspNetCore.Mvc;

namespace AccessTeamWeb.Areas.Client.Controller
{
    [Area("Client")]
    public class ClientController : Microsoft.AspNetCore.Mvc.Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}