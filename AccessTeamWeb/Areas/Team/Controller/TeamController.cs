﻿using System;
using System.Threading.Tasks;
using AccessTeamWeb.Areas.Team.ViewModel;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using Team.Dto;
using Team.Repository.Interfaces;
using Team.Services.Interfaces;

namespace AccessTeamWeb.Areas.Team.Controller
{
    [Area("Team")]
    public class TeamController : Microsoft.AspNetCore.Mvc.Controller
    {
        private readonly IToastNotification _notification;
        private readonly ITeamRepository _teamRepository;
        private readonly ITeamServices _teamServices;

        public TeamController(IToastNotification notification, ITeamRepository teamRepository,
            ITeamServices teamServices)
        {
            _notification = notification;
            _teamRepository = teamRepository;
            _teamServices = teamServices;
        }

        public async Task<IActionResult> Index()
        {
            var teams = await _teamRepository.GetAllAsync();
            return View(teams);
        }

        public IActionResult New()
        {
            try
            {
                return View(new TeamVm());
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return RedirectToAction(nameof(Index));
            }
        }

        [HttpPost]
        public async Task<IActionResult> New(TeamVm vm)
        {
            try
            {
                if (!ModelState.IsValid) return View(vm);

                var dto = new TeamDto
                {
                    Name = vm.Name,
                    Description = vm.Description
                };
                await _teamServices.Create(dto);
                _notification.AddSuccessToastMessage($"{dto.Name} has been Created");
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return View(vm);
            }
        }

        public async Task<IActionResult> Edit(long id)
        {
            try
            {
                var team = await _teamRepository.FindOrThrowAsync(id);
                var vm = new TeamVm
                {
                    Name = team.Name,
                    Description = team.Description
                };
                return View(vm);
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return RedirectToAction(nameof(Index));
            }
        }

        public async Task<IActionResult> Edit(long id, TeamVm vm)
        {
            try
            {
                if (!ModelState.IsValid) return View(vm);

                var team = await _teamRepository.FindAsync(id);
                var dto = new TeamDto
                {
                    Name = vm.Name,
                    Description = vm.Description
                };
                await _teamServices.Update(team, dto);
                _notification.AddSuccessToastMessage($"{dto.Name} is Updated");
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return RedirectToAction(nameof(Index));
            }
        }

        public async Task<IActionResult> Remove(long id)
        {
            try
            {
                var team = await _teamRepository.FindOrThrowAsync(id);
                await _teamServices.Remove(team);
                _notification.AddSuccessToastMessage($"{team.Name} has been deleted");
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return RedirectToAction(nameof(Index));
            }
        }
    }
}