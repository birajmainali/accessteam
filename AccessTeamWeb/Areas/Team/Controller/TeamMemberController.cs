﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AccessTeamWeb.Areas.Team.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NToastNotify;
using Team.Repository.Interfaces;
using Team.Services.Interfaces;
using User.Repository.Interface;

namespace AccessTeamWeb.Areas.Team.Controller
{
    [Area("Team")]
    public class TeamMemberController : Microsoft.AspNetCore.Mvc.Controller
    {
        private readonly ITeamMemberRepository _memberRepository;
        private readonly ITeamMemberService _memberService;
        private readonly IToastNotification _notification;
        private readonly ITeamRepository _teamRepository;
        private readonly IUserRepository _userRepository;

        public TeamMemberController(IToastNotification notification, ITeamMemberRepository memberRepository,
            ITeamMemberService memberService, ITeamRepository teamRepository, IUserRepository userRepository)
        {
            _notification = notification;
            _memberRepository = memberRepository;
            _memberService = memberService;
            _teamRepository = teamRepository;
            _userRepository = userRepository;
        }

        public async Task<IActionResult> Index()
        {
            var members = await _memberRepository.GetAllAsync();
            return View(members);
        }

        public async Task<IActionResult> AddMember(long id)
        {
            try
            {
                var team = await _teamRepository.FindOrThrowAsync(id);
                var assignedUsers = await _memberRepository.GetQueryable()
                    .Where(x => x.TeamId == id)
                    .Select(x => x.UserId).ToListAsync();
                var users = await _userRepository.GetAllAsync(x => x.IsApprove && !assignedUsers.Contains(x.Id));
                return View(new AddTeamMemberVm
                {
                    Team = team,
                    Users = users
                });
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return RedirectToAction("Index");
            }
        }

        public async Task<IActionResult> Remove(long id)
        {
            try
            {
                var member = await _memberRepository.FindOrThrowAsync(id);
                await _memberService.Remove(member);
                _notification.AddSuccessToastMessage("Removed");
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _notification.AddErrorToastMessage(e.Message);
                return RedirectToAction(nameof(Index));
            }
        }
    }
}