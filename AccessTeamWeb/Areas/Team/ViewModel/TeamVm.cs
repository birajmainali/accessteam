﻿using System.ComponentModel.DataAnnotations;

namespace AccessTeamWeb.Areas.Team.ViewModel
{
    public class TeamVm
    {
        [Required] public string Name { get; set; }

        public string? Description { get; set; }
    }
}