﻿using System.Collections.Generic;

namespace AccessTeamWeb.Areas.Team.ViewModel
{
    public class AddTeamMemberVm
    {
        public global::Team.Entity.Team Team { get; set; }

        public List<global::User.Entity.User> Users { get; set; }
    }
}