﻿using System.Collections.Generic;

namespace AccessTeamWeb.Areas.API.ViewModels.Modules
{
    public class AddModuleVm
    {
        public long ProductId { get; set; }
        public List<string> Modules{ get; set; }
    }
}