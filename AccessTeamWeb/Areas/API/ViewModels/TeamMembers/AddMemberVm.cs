﻿using System.Collections.Generic;

namespace AccessTeamWeb.Areas.API.ViewModels.TeamMembers
{
    public class AddMemberVm
    {
        public long TeamId { get; set; }
        public List<long> UserIds { get; set; }
    }
}