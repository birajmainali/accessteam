﻿using System;
using System.Threading.Tasks;
using AccessTeamWeb.Areas.API.ViewModels.Modules;
using Microsoft.AspNetCore.Mvc;
using Settings.Dto;
using Settings.Repository.Interface;
using Settings.Services.Interface;

namespace AccessTeamWeb.Areas.API.Controller
{
    [Area("API")]
    public class ProductModulesController : Microsoft.AspNetCore.Mvc.Controller
    {
        private readonly IModuleService _moduleService;
        private readonly IProductRepository _productRepository;

        public ProductModulesController(IModuleService moduleService, IProductRepository productRepository)
        {
            _moduleService = moduleService;
            _productRepository = productRepository;
        }

        [HttpPost("/API/Setup/Module/Add")]
        public async Task<IActionResult> AddModule([FromBody] AddModuleVm vm)
        {
            try
            {
                var product = await _productRepository.FindOrThrowAsync(vm.ProductId);
                var dto = new BulkModuleAddDto(product, vm.Modules);
                await _moduleService.CreateBulk(dto);
                return Ok(new
                {
                    success = true
                });
            }
            catch (Exception e)
            {
                return BadRequest(new
                {
                    e.Message
                });
            }
        }
    }
}