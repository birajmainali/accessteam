﻿using System;
using System.Threading.Tasks;
using AccessTeamWeb.Areas.API.ViewModels.TeamMembers;
using Microsoft.AspNetCore.Mvc;
using Team.Dto;
using Team.Repository.Interfaces;
using Team.Services.Interfaces;
using User.Repository.Interface;

namespace AccessTeamWeb.Areas.API.Controller
{
    [Area("API")]
    public class TeamMemberController : Microsoft.AspNetCore.Mvc.Controller
    {
        private readonly ITeamMemberService _teamMemberService;
        private readonly ITeamRepository _temRepository;
        private readonly IUserRepository _userRepo;

        public TeamMemberController(ITeamRepository temRepository, IUserRepository userRepo,
            ITeamMemberService teamMemberService)
        {
            _temRepository = temRepository;
            _userRepo = userRepo;
            _teamMemberService = teamMemberService;
        }

        [HttpPost("/Api/Team/Member/Add")]
        public async Task<IActionResult> AddMembers([FromBody] AddMemberVm vm)
        {
            try
            {
                var team = await _temRepository.FindOrThrowAsync(vm.TeamId);
                var users = await _userRepo.GetAllAsync(x => vm.UserIds.Contains(x.Id));
                if (users.Count != vm.UserIds.Count) throw new Exception("User not found");

                var dto = new BulkTeamMemberAddDto(team, users);
                await _teamMemberService.CreateBulk(dto);
                return Ok(new
                {
                    success = true
                });
            }
            catch (Exception e)
            {
                return BadRequest(new
                {
                    e.Message
                });
            }
        }
    }
}