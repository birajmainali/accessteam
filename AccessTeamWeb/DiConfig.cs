﻿using AccessTeamWeb.Data;
using AccessTeamWeb.Manager;
using AccessTeamWeb.Manager.Interfaces;
using Base;
using Base.Extensions;
using Client;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NToastNotify;
using Settings;
using Team;
using User;
using Work;

namespace AccessTeamWeb
{
    public static class DiConfig
    {
        public static IServiceCollection UseAccessTeam(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseNpgsql(
                    configuration.GetDefaultConnectionString()));
            services.AddControllersWithViews().AddRazorRuntimeCompilation().AddNToastNotifyNoty(new NotyOptions
            {
                ProgressBar = true,
                Timeout = 1000,
                Theme = "mint"
            });
            services.AddScoped<DbContext, ApplicationDbContext>();
            services.UseBase().UseClient().UseTeam().UseWork().UseUser().UseSettings();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IAuthManager, AuthManager>();
            return services;
        }
    }
}